package com.epaylater;

/**
 * @author Akshay Viswanathan
 */

import com.google.gson.Gson;
import org.glassfish.jersey.message.internal.ReaderWriter;
import redis.clients.jedis.Jedis;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@Path("/spend")
@Produces(MediaType.APPLICATION_JSON)
public class SpendResource {
    private final String template;

    SpendResource(final String template) {
        this.template = template;
    }

    /**
     * Log an expenditure
     * <p>
     * Client needs to be authorized in order for this to happen.
     *
     * @param requestContext incoming request
     * @return returns HTTP response as per the execution.
     */
    @POST
    @AuthorizationRequired
    @Consumes(MediaType.APPLICATION_JSON)
    public Response spend(final ContainerRequestContext requestContext) {
        final String body = getEntityBody(requestContext);
        final SpendTransaction spendTransaction = new Gson().fromJson(body, SpendTransaction.class);
        final Jedis jedis = new Jedis();
        final Map<String, String> creditLimitMap = jedis.hgetAll("credit_limit");
        //Assume that minimum credit limit for someone without credit history is 2000
        final int creditLimit = creditLimitMap.get(spendTransaction.getPhoneNumber()) == null ? 2000 : Integer.parseInt(creditLimitMap.get(spendTransaction.getPhoneNumber()));

        //Respond with the credit limit of the person
        return Response.ok(creditLimit).build();
    }

    /**
     * Extract the body of the incoming request
     *
     * @param requestContext request context containing the body and the headers
     * @return body of the incoming request as a raw string
     */
    private static String getEntityBody(final ContainerRequestContext requestContext) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final InputStream in = requestContext.getEntityStream();

        final StringBuilder stringBuilder = new StringBuilder();
        try {
            ReaderWriter.writeTo(in, out);

            final byte[] requestEntity = out.toByteArray();
            if (requestEntity.length == 0) {
                stringBuilder.append("").append("\n");
            } else {
                stringBuilder.append(new String(requestEntity)).append("\n");
            }

            requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));
        } catch (final IOException ex) {
            //Handle logging error
        }
        return stringBuilder.toString();
    }
}

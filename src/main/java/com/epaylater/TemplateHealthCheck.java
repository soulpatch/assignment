package com.epaylater;

import com.codahale.metrics.health.HealthCheck;

/**
 * @author Akshay Viswanathan
 */
public class TemplateHealthCheck extends HealthCheck {
    private final String template;

    public TemplateHealthCheck(final String template) {
        this.template = template;
    }

    @Override
    protected Result check() throws Exception {
        try {
            Integer.parseInt(template);
        } catch (final NumberFormatException nfe) {
            return Result.unhealthy("template doesn't include a name");
        }

        return Result.healthy();
    }
}

package com.epaylater;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * @author Akshay Viswanathan
 */
public class LoginApplication extends Application<LoginConfiguration> {
    public static void main(final String[] args) throws Exception {
        new LoginApplication().run(args);
    }

    @Override
    public void run(final LoginConfiguration configuration, final Environment environment) throws Exception {
        final LoginResource loginResource = new LoginResource(configuration.getTemplate());
        environment.jersey().register(loginResource);
        final SpendResource spendResource = new SpendResource(configuration.getTemplate());
        environment.jersey().register(spendResource);

        environment.jersey().register(AuthorizationFeature.class);

//        final TemplateHealthCheck healthCheck = new TemplateHealthCheck(configuration.getTemplate());
//        environment.healthChecks().register("template", healthCheck);
    }

    @Override
    public String getName() {
        return "Login";
    }

    @Override
    public void initialize(final Bootstrap<LoginConfiguration> bootstrap) { // nothing to do yet
    }
}

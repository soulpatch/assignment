package com.epaylater;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

/**
 * @author Akshay Viswanathan
 */
public class AuthorizationFeature implements DynamicFeature {
    @Override
    public void configure(final ResourceInfo resourceInfo, final FeatureContext context) {
        if (resourceInfo.getResourceMethod().getAnnotation(AuthorizationRequired.class) != null) {
            context.register(AuthorizationFilter.class);
        }
    }
}

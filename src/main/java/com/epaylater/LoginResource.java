package com.epaylater;

import redis.clients.jedis.Jedis;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

/**
 * Resource for user login
 *
 * @author Akshay Viswanathan
 */
@Path("/login")
@Produces(MediaType.APPLICATION_JSON)
public class LoginResource {
    private final String template;

    LoginResource(final String template) {
        this.template = template;
    }

    /**
     * Login using phone number
     *
     * @param phoneNum phone number of the user
     * @return UUID token for future authorization
     */
    @POST
    public Token login(@QueryParam("phone") final String phoneNum) {
        final UUID uuid = UUID.randomUUID();
        final String value = String.format(template, uuid.toString());
        final Jedis jedis = new Jedis();
        jedis.hset("tokens", phoneNum, uuid.toString());
        return new Token(value);
    }
}

package com.epaylater;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Token to be returned when a phone is provided
 *
 * @author Akshay Viswanathan
 */
public class Token {
    private String token;

    public Token(final String token) {
        this.token = token;
    }

    @JsonProperty
    public String getToken() {
        return token;
    }
}

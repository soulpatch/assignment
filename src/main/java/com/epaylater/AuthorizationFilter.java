package com.epaylater;

import redis.clients.jedis.Jedis;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * @author Akshay Viswanathan
 */
public class AuthorizationFilter implements ContainerRequestFilter {
    @Override
    public void filter(final ContainerRequestContext requestContext) throws IOException {
        final String tokenHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (tokenHeader == null) {
            final Exception cause = new IllegalArgumentException("Date Header was not specified");
            throw new WebApplicationException(cause, Response.Status.BAD_REQUEST);
        } else {
            final Jedis jedis = new Jedis();
            final Map<String, String> tokenMap = jedis.hgetAll("tokens");
            final Collection<String> tokens = tokenMap.values();

            if (!tokens.contains(tokenHeader)) {
                System.out.println("Could not find token");
                final Exception cause = new IllegalArgumentException("Token could not be verified");
                throw new WebApplicationException(cause, Response.Status.UNAUTHORIZED);
            }
        }
    }
}

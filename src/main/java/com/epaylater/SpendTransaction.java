package com.epaylater;

/**
 * @author Akshay Viswanathan
 */
public class SpendTransaction {
    private String phoneNumber;
    private String transaction_date;
    private String description;
    private int amount;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(final String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(final int amount) {
        this.amount = amount;
    }
}

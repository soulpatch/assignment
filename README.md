# assignment

How to start the assignment application
---

1. Run `mvn clean package` to build your application
1. Start application with `java -jar target/assignment-1.0-SNAPSHOT.jar server src/main/java/com/epaylater/token.yml`
1. To check that your application is running enter url `http://localhost:8080`
1. To login `http://localhost:8080/login?phone=1122331122`
1. To log expense `http://localhost:8080/spend` with expense in the body as a json like `{"transaction_date":"2017-12-20T03:30:00", "description":"Dining", "amount":2000}`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
